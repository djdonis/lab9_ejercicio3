﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab10_2
{
    public partial class Form1 : Form
    {
        int[] Arreglo;
        int iTamanio, iNum=0;

        public Form1()
        {
            InitializeComponent();
        }


        //################################### PROCEDIMIENTOS ##############################

        private void ImprimirArreglo()
        {
            for (int i = 0; i < Arreglo.Length; i++)
                lbResultados.Items.Add(Arreglo[i]);
        }

        private void SumaArreglo()
        {
            int iSuma = 0;

            for (int i = 0; i < Arreglo.Length; i++)
                iSuma += Arreglo[i];

            lbResultados.Items.Add("La suma de todos los números del arreglo es: " + iSuma.ToString());
        }

        private void SumaPosPares()
        {
            int iSuma = 0;
            for (int i = 0; i < Arreglo.Length; i=i+2)
                iSuma += Arreglo[i];

            lbResultados.Items.Add("La suma de los valores en posiciones pares es: " + iSuma.ToString());
        }

        private void SumaPosImpares()
        {
            int iSuma = 0;
            for (int i = 1; i < Arreglo.Length; i = i + 2)
                iSuma += Arreglo[i];

            lbResultados.Items.Add("La suma de los valores en posiciones pares es: " + iSuma.ToString());
        }

        private void OrdenarArreglo()
        {
            int temp = 0;

            for (int k = 0; k < Arreglo.Length; k++)
            {
                for (int i = 0; i < Arreglo.Length - 1; i++)
                {
                    if (Arreglo[i] > Arreglo[i + 1])
                    {
                        temp = Arreglo[i + 1];
                        Arreglo[i + 1] = Arreglo[i];
                        Arreglo[i] = temp;
                    }
                }
            }
        }

        //################################### BOTONES #####################################
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (mtbCantidad.Text == "")
                MessageBox.Show("Debe ingresar un número entero.");
            else
            {

                iTamanio = Convert.ToInt32(mtbCantidad.Text);
                Arreglo = new int[iTamanio];
                label1.Enabled = false;
                mtbCantidad.Enabled = false;
                btnAceptar.Enabled = false;
                MessageBox.Show("Ha creado exitosamente un Arreglo de tamaño " + iTamanio.ToString());
                label2.Enabled = true;
                mtbValor.Enabled = true;
                btnInsertar.Enabled = true;
            }
        }

        private void btnMostrar_Click(object sender, EventArgs e)
        {
            lbResultados.Items.Clear();
            ImprimirArreglo();
            SumaArreglo();
            lbResultados.Items.Add("La longitud del arreglo es: " + Arreglo.Length.ToString());
            SumaPosPares();
            SumaPosImpares();
        }

        private void btnOrdenar_Click(object sender, EventArgs e)
        {
            lbResultados.Items.Clear();
            OrdenarArreglo();
            ImprimirArreglo();
        }

        private void btnAgregarFila_Click(object sender, EventArgs e)
        {
            dgv_Tablero.ColumnHeadersVisible = false;
            dgv_Tablero.Rows.Add(); //Agrega una fila al DataGridView
            int numFilas = dgv_Tablero.Rows.Count;  //Guarda el número de filas que tiene el DataGridView en ese momento.
            dgv_Tablero.Rows[numFilas - 1].Height = 50; //Altura da la fila del DataGrid igual a 50

            if(numFilas%2==1)   //Si es fila impar, comienza pintando la columna 0 de negro.
            {
                for (int j = 0; j < 8; j+=2)
                {
                    dgv_Tablero.Rows[numFilas - 1].Cells[j].Style.BackColor = Color.Black;
                }
            }
            else //Si es fila par, comienza pintando la columna 1 de negro.
            {
                for (int j = 1; j < 8; j += 2)  
                {
                    dgv_Tablero.Rows[numFilas - 1].Cells[j].Style.BackColor = Color.Black;
                }
            }
            
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            try
            {
                int iValorIngresado = Convert.ToInt32(mtbValor.Text);
                Arreglo[iNum] = iValorIngresado;
                lbResultados.Items.Add(Arreglo[iNum]);
                iNum++;
                if(iNum == Arreglo.Length)
                {
                    label2.Enabled = false;
                    mtbValor.Enabled = false;
                    btnInsertar.Enabled = false;
                    MessageBox.Show("Ha ingresado la cantidad de números permitida.");
                    btnMostrar.Enabled = true;
                    btnOrdenar.Enabled = true;
                }
            }
            catch
            {
                MessageBox.Show("Debe ingresar un número entero.");
            }
        }
    }
}
